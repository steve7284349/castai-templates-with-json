#!/bin/bash
BASE_DIR="$(pwd)/config"

echo "Base directory is $BASE_DIR"

# Function to handle API calls
call_api() {
    local url="$1"
    local output_file="$2"
    local response

    response=$(curl --silent --request GET \
        --url "$url" \
        --header "X-API-Key: $CAST_API_KEY" \
        --header 'accept: application/json')

    # Check if response is empty
    if [ -z "$response" ]; then
        echo "Error: Empty response received for $url"
        exit 1
    fi

    echo "$response" | jq > "$output_file"
}

# Make the API call to get the list of clusters
call_api "https://api.cast.ai/v1/kubernetes/external-clusters" "/tmp/clusters.json"

echo $response

# Parse the response and loop through the clusters
clusters=$(cat /tmp/clusters.json  | jq -r '.items[].id')
for cluster_id in $clusters; do
    # Check each cluster status
    cluster_status=$(cat /tmp/clusters.json | jq -r --arg id "$cluster_id" '.items[] | select(.id == $id) | .status')
    cluster_name=$(cat /tmp/clusters.json | jq -r --arg id "$cluster_id" '.items[] | select(.id == $id) | .name')
    CLUSTER_DIR="$BASE_DIR/$cluster_name"
    if [ -d "$CLUSTER_DIR" ]; then
        echo "Folder $CLUSTER_DIR exists. Removing it..."
        rm -rf "$CLUSTER_DIR"
        echo "Folder $CLUSTER_DIR removed successfully."
    else
        echo "Folder $CLUSTER_DIR does not exist."
    fi
    mkdir -p "$CLUSTER_DIR"
    echo "Cluster ID: $cluster_id, Status: $cluster_status, Name: $cluster_name, Dir: $CLUSTER_DIR"
    
    # Get Autoscaler Settings
    call_api "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/policies" "$CLUSTER_DIR/policies.json"
    
    # Get Node Configurations
    mkdir -p $CLUSTER_DIR/node-configs
    call_api "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/node-configurations" "/tmp/node_configs.json"
    node_configs=$(cat /tmp/node_configs.json  | jq -r '.items[].id')
    for id in $node_configs; do
        echo Getting Node Config: $id
        config=$(cat /tmp/node_configs.json | jq -r --arg id "$id" '.items[] | select(.id == $id) | .')
        config_name=$(cat /tmp/node_configs.json | jq -r --arg id "$id" '.items[] | select(.id == $id) | .name')
        stripped_config=$(echo $config | jq 'del(.version)' )
        echo $stripped_config | jq > $CLUSTER_DIR/node-configs/$config_name.json
    done
    # Get Node Templates
    mkdir -p $CLUSTER_DIR/node-templates
    call_api "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/node-templates?includeDefault=true" "/tmp/node_templates.json"
    node_templates=$(cat /tmp/node_templates.json  | jq -r '.items[].template.name')
    echo $node_templates
    for name in $node_templates; do
        echo Getting Node Template: $name
        config=$(cat /tmp/node_templates.json | jq -r --arg name "$name" '.items[] | select(.template.name == $name) | .')
        stripped_config=$(echo $config | jq 'del(.template.version)' )
        echo $stripped_config | jq .template > $CLUSTER_DIR/node-templates/$name.json
    done
    mkdir -p $CLUSTER_DIR/rebalancing-jobs

    call_api "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/rebalancing-jobs" "/tmp/rebalancing_jobs.json"
    rebalancing_jobs=$(cat /tmp/rebalancing_jobs.json  | jq -r '.jobs[].id')
    for id in $rebalancing_jobs; do
        echo Getting Rebalancer Job: $id
        config=$(cat /tmp/rebalancing_jobs.json | jq -r --arg id "$id" '.jobs[] | select(.id == $id) | .')
        stripped_config=$(echo $config | jq 'del(.nextTriggerAt)' )
        echo $stripped_config | jq > $CLUSTER_DIR/rebalancing-jobs/$id.json
    done
done

REBALANCE_DIR="$BASE_DIR/rebalancing-schedules"
if [ -d "$REBALANCE_DIR" ]; then
    echo "Folder $REBALANCE_DIR exists. Removing it..."
    rm -rf "$REBALANCE_DIR"
    echo "Folder $REBALANCE_DIR removed successfully."
else
    echo "Folder $REBALANCE_DIR does not exist."
fi
mkdir -p $REBALANCE_DIR

# Get Rebalance Schedules
call_api "https://api.cast.ai/v1/rebalancing-schedules" "/tmp/rebalancing-schedules.json"
rebalance_schedule=$(cat /tmp/rebalancing-schedules.json  | jq -r '.schedules[].id')
for id in $rebalance_schedule; do
    echo Getting Rebalance Schedule: $id
    set -o noglob
    config=$(cat /tmp/rebalancing-schedules.json | jq --arg id "$id" '.schedules[] | select(.id == $id) | .')
    config_name=$(cat /tmp/rebalancing-schedules.json | jq --arg id "$id" '.schedules[] | select(.id == $id) | .name')
    stripped_config=$(echo $config | jq 'del(.nextTriggerAt)' | jq 'del(.jobs[].nextTriggerAt)' |  jq 'del(.jobs[].rebalancingPlanId)' |  jq 'del(.jobs[])')
    echo $stripped_config | jq . > $REBALANCE_DIR/$(echo $config_name | tr -d ' ' | tr -d '"').json
done
