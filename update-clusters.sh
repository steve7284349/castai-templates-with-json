#!/bin/bash
BASE_DIR="$(pwd)/config"

echo "Base directory is $BASE_DIR"

# Function to handle API calls
call_api() {
    local url="$1"
    local output_file="$2"
    local response

    response=$(curl --silent --request GET \
        --url "$url" \
        --header "X-API-Key: $CAST_API_KEY" \
        --header 'accept: application/json')

    # Check if response is empty
    if [ -z "$response" ]; then
        echo "Error: Empty response received for $url"
        exit 1
    fi

    echo "$response" | jq > "$output_file"
}

for file in "$BASE_DIR"/rebalancing-schedules/*.json; do
    if [ -f "$file" ]; then
        id=$(jq -r '.id' "$file")
        echo Updating Rebalancer Schedule: $id
        curl -X PUT --location "https://api.cast.ai/v1/rebalancing-schedules?id=$id" \
            --header 'Content-Type: application/json' \
            --header 'Accept: application/json' \
            --header "X-API-Key: $CAST_API_KEY" \
            --data @$file
    fi
done


# Make the API call to get the list of clusters
call_api "https://api.cast.ai/v1/kubernetes/external-clusters" "/tmp/clusters.json"
# Parse the response and loop through the clusters
clusters=$(cat /tmp/clusters.json  | jq -r '.items[].id')
for cluster_id in $clusters; do
    # Check each cluster status
    cluster_status=$(cat /tmp/clusters.json | jq -r --arg id "$cluster_id" '.items[] | select(.id == $id) | .status')
    cluster_name=$(cat /tmp/clusters.json | jq -r --arg id "$cluster_id" '.items[] | select(.id == $id) | .name')
    CLUSTER_DIR="$BASE_DIR/$cluster_name"
    if [ -d "$CLUSTER_DIR" ]; then 
        mkdir -p "$CLUSTER_DIR"
        echo "Cluster ID: $cluster_id, Status: $cluster_status, Name: $cluster_name, Dir: $CLUSTER_DIR"
        for file in "$CLUSTER_DIR"/node-templates/*.json; do
            if [ -f "$file" ]; then
                name=$(jq -r '.name' "$file")
                echo Updating Template: $name on $cluster_name
                curl -X PUT --location "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/node-templates/$name" \
                    --header 'Content-Type: application/json' \
                    --header 'Accept: application/json' \
                    --header "X-API-Key: $CAST_API_KEY" \
                    --data @$file
            fi
        done
        for file in "$CLUSTER_DIR"/node-configs/*.json; do
            if [ -f "$file" ]; then
                id=$(jq -r '.id' "$file")
                echo Updating Node Config: $id on $cluster_name
                curl -X POST --location "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/node-configurations/$id" \
                    --header 'Content-Type: application/json' \
                    --header 'Accept: application/json' \
                    --header "X-API-Key: $CAST_API_KEY" \
                    --data @$file
            fi
        done
        for file in "$CLUSTER_DIR"/rebalancing-jobs/*.json; do
            if [ -f "$file" ]; then
                id=$(jq -r '.id' "$file")
                echo Updating Rebalancer Job: $id on $cluster_name
                curl -X PUT --location "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/rebalancing-jobs/$id" \
                    --header 'Content-Type: application/json' \
                    --header 'Accept: application/json' \
                    --header "X-API-Key: $CAST_API_KEY" \
                    --data @$file
            fi
        done
        echo "Updating Cluster Policies"
        curl -X PUT --location "https://api.cast.ai/v1/kubernetes/clusters/$cluster_id/policies" \
            --header 'Content-Type: application/json' \
            --header 'Accept: application/json' \
            --header "X-API-Key: $CAST_API_KEY" \
            --data @$CLUSTER_DIR/policies.json
    else
        echo "Folder $CLUSTER_DIR does not exist."
    fi
done